// tạo ra 1 async action để fetch ds khóa học
import axios from "axios";
import { createAction } from ".";
import { actionType } from "./type";
export const fetchCourses = (page) => {
  return async (dispatch) => {
    try {
      const res = await axios({
        url: `http://movieapi.cyberlearn.vn/api/QuanLyPhim/LayDanhSachPhimPhanTrang?maNhom=GP01&soTrang=${page}&soPhanTuTrenTrang=16`,

        method: "GET",
      });
      dispatch(createAction(actionType.SET_COURSES, res.data.content));
    } catch (err) {
      console.log(err);
    }
  };
};

export const fetchCourse = (id) => {
  return async (dispatch) => {
    try {
      const res = await axios({
        url: "http://movieapi.cyberlearn.vn/api/QuanLyPhim/LayThongTinPhim",
        method: "GET",
        params: {
          maPhim: id,
        },
      });
      dispatch(createAction(actionType.SELECTED_PRODUCT, res.data.content));
    } catch (err) {
      console.log(err);
    }
  };
};
