import axios from "axios";
import { createAction } from ".";
import { actionType } from "./type";

export const signin = (userLogin, callback) => {
  return async (dispatch) => {
    try {
      const res = await axios({
        method: "POST",
        url: "http://movieapi.cyberlearn.vn/api/QuanLyNguoiDung/DangNhap",
        data: userLogin,
      });
      // console.log(res);
      dispatch(createAction(actionType.SET_ME, res.data.content));
      console.log(res);
      localStorage.setItem("t", res.data.content.accessToken);

      callback();
    } catch (err) {
      alert(err.response.data.content);
    }
  };
};

export const signup = (info, callback) => {
  return async (dispatch) => {
    try {
      const res = await axios({
        method: "POST",
        url: "http://movieapi.cyberlearn.vn/api/QuanLyNguoiDung/DangKy",
        data: info,
      });
      // console.log(res);
      console.log(res);
      callback();
    } catch (err) {
      alert(err.response.data.content);
    }
  };
};

export const fetchMe = async (dispatch) => {
  try {
    const res = await axios({
      url: "http://movieapi.cyberlearn.vn/api/QuanLyNguoiDung/ThongTinTaiKhoan",
      method: "POST",
      headers: {
        Authorization: "Bearer " + localStorage.getItem("t"),
      },
    });
    // console.log(res);
    dispatch(createAction(actionType.SET_ME, res.data.content));
  } catch (err) {
    console.log(err);
  }
};
