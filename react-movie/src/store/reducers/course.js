import { actionType } from "../action/type";

const initialState = {
  courses: {
    currentPage: 1,
    count: "",
    totalPages: "",
    totalCount: "",
    items: [],
  },
  product: null, // để là null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionType.SET_COURSES:
      state.courses = action.payload;
      return { ...state };
    case actionType.SELECTED_PRODUCT:
      state.product = action.payload;
      return { ...state };
    default:
      return state;
  }
};
export default reducer;
