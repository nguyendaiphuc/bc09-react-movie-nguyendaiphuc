import React from "react";
import { NavLink, useHistory } from "react-router-dom";
import {
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  Button,
} from "@material-ui/core";
import MovieOutlinedIcon from "@material-ui/icons/MovieOutlined";
import useStyle from "./style";
import { useSelector } from "react-redux";
import PermIdentityIcon from "@material-ui/icons/PermIdentity";
const Header = () => {
  const history = useHistory();
  const classes = useStyle();
  const {
    title,
    navLink,
    active,
    appBar,
    appBarWrapper,
    colorTitle,

    appBarIcon,
  } = classes;
  const me = useSelector((state) => state.me);
  return (
    <AppBar className={appBar} elevation={0} position="sticky">
      {/* <video
        className={appBarVideo}
        src="blob:https://www.bilibili.com/8414b609-c268-460f-b5d7-0e0b296e4ee4"
        width="1689"
        height="158"
      ></video> */}
      <Toolbar className={appBarWrapper}>
        <IconButton
          onClick={() => {
            history.push("/");
          }}
          edge="start"
          aria-label="menu"
        >
          <MovieOutlinedIcon className={appBarIcon} />
        </IconButton>
        <Typography className={title} variant="h5">
          My <span className={colorTitle}> Movies</span>
        </Typography>

        <NavLink className={navLink} exact activeClassName={active} to="/">
          Home
        </NavLink>

        {me ? (
          <>
            <Button
              onClick={() => history.push("/profile")}
              style={{ marginLeft: 20 }}
            >
              {me.hoTen}
              <PermIdentityIcon />
            </Button>
          </>
        ) : (
          <NavLink className={navLink} activeClassName={active} to="signin">
            <span>Sign In</span>
            <PermIdentityIcon />
          </NavLink>
        )}
      </Toolbar>
    </AppBar>
  );
};

export default Header;
