import React, { useEffect, useCallback } from "react";
import { CardMedia, Container, Grid, makeStyles } from "@material-ui/core";
import { Typography } from "antd";
import { useDispatch, useSelector } from "react-redux";
import Layout from "../../HOCs/Layout";
import { fetchCourse } from "../../store/action/course";

const Detail = (props) => {
  const dispatch = useDispatch();
  const { id } = props.match.params;
  const getInfoFilm = useCallback(() => {
    dispatch(fetchCourse(id));
  }, [dispatch, id]);
  useEffect(() => {
    getInfoFilm();
  }, [dispatch, getInfoFilm]);
  const { product } = useSelector((state) => state.course);
  const { tenPhim, hinhAnh, moTa, trailer } = product || {};

  const useStyles = makeStyles(() => {
    return {
      wrapper: {
        height: "90vh",
        padding: "50px 0",
        textAlign: "center",
        // margin: "0 auto",
      },
      imgDefault: {
        backgroundImage: `url(${hinhAnh}), url("http://movieapi.cyberlearn.vn/hinhanh/diep-van_gp01.jpg")`,
        height: "75vh",
        borderRadius: "20px",
      },
      textLeft: {
        textAlign: "left",
      },
      textTheme: {
        letterSpacing: "2px",
        padding: " 10px 0",
        fontSize: "1rem",
      },
    };
  });
  const classes = useStyles();

  return (
    <Layout>
      <Container className={classes.wrapper} maxWidth="lg">
        <Grid container spacing={5}>
          <Grid item md={5}>
            <CardMedia className={classes.imgDefault} />
          </Grid>
          <Grid item md={7}>
            <Typography spacing={5} variant="h2" component="h1">
              {tenPhim}
            </Typography>
            <div className={classes.textLeft}>
              <tr className={classes.rowStyle}>
                <td valign="middle" width="100px">
                  <Typography component="h3">Mô Tả:</Typography>
                </td>
                <td>
                  <Typography className={classes.textTheme}>{moTa}</Typography>
                </td>
              </tr>

              <tr className={classes.rowStyle}>
                <td valign="middle" width="100px">
                  <Typography component="h3">Trailer:</Typography>
                </td>
                <td>
                  <Typography className={classes.textTheme}>
                    {trailer}
                  </Typography>
                </td>
              </tr>
            </div>
          </Grid>
        </Grid>
      </Container>
    </Layout>
  );
};

export default Detail;
