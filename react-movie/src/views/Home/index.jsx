import React, { useCallback, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Container, Grid, Typography } from "@material-ui/core";
import Course from "../../components/Course";
import { fetchCourses } from "../../store/action/course";

import Layout from "../../HOCs/Layout";
import { Pagination } from "antd";
import "antd/dist/antd.css";

const Home = () => {
  const [page, setPage] = useState(1);
  const dispatch = useDispatch();
  const { courses } = useSelector((state) => state.course);
  // console.log(courses.items);

  const managerFilm = useCallback(() => {
    dispatch(fetchCourses(page));
    // console.log(page);
  }, [page, dispatch]);

  useEffect(() => {
    managerFilm();
  }, [page, managerFilm]);

  return (
    <Layout>
      <Container>
        <Typography component="h1" variant="h3" align="center">
          Hot Movies
        </Typography>
        <Grid container spacing={3}>
          {courses.items.map((item) => {
            return (
              <Grid item md={3} key={item.maPhim}>
                <Course item={item} />
              </Grid>
            );
          })}
        </Grid>

        <Pagination
          style={{ paddingTop: "20px" }}
          pageSize={courses?.count}
          total={courses?.totalCount}
          onChange={(page) => {
            setPage(page);
          }}
          className="page"
        />
      </Container>
    </Layout>
  );
};

export default Home;
