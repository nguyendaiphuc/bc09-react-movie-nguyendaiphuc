import { makeStyles } from "@material-ui/core";

const styles = makeStyles((theme) => {
  return {
    "@global": {},
    title: {
      color: "#fff",
      fontSize: "2.5rem",
      letterSpacing: "10px",
      fontWeight: " 700",
      with: "100%",
      alignItems: "center",
    },
    wrapper: {
      background:
        "url('https://p.w3layouts.com/demos_new/template_demo/28-07-2017/online_login_form-demo_Free/1930595034/web/images/1.jpg')",
      backgroundSize: "cover",
      width: "100%",
      height: "100vh",
      backgroundRepeat: "no-repeat",
      backgroundPosition: "center",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      margin: " auto",
      textAlign: "center",
    },
    formContent: { padding: " 10px 0", color: "#fff" },
    themeInput: {
      border: "1px solid transparent",
      background: "rgba(255, 255, 255, 0.08)",
      borderRadius: 40,
      height: 50,
      color: "#F1F7E7",
      padding: "20px ",
      margin: " 10px 0",
      transition: "all 0.5s",
      width: " 100%",
      "&::placeholder ": {
        color: "#F1F7E7",
      },
      "&:focus ": {
        outline: " none",
      },
      "&:hover": {
        border: "1px solid ",
      },
    },
    themeButton: {
      background: "#df5a5a",
      width: "100%",
      padding: 10,
      borderRadius: 40,
      cursor: "pointer",
      border: "none",
    },
    container: {
      width: "60vh",
    },

    h5Theme: {
      color: "#D4ECDD",
      marginTop: "20px",
      cursor: "pointer",
      textAlign: "right",
    },
  };
});
export default styles;
