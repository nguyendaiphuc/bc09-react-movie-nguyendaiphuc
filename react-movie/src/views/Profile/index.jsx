import { Container, Typography } from "@material-ui/core";
import React from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import Layout from "../../HOCs/Layout";
import useStyle from "./style";

const Profile = () => {
  const me = useSelector((state) => state.me);

  console.log(me.hoTen);
  const history = useHistory();
  const classes = useStyle();
  const { wrapper, userImg, img, infoContent, infoSpacing } = classes;
  return (
    <Layout>
      <Container>
        <div className={wrapper}>
          <div className={userImg}>
            <img
              className={img}
              src="https://gamenoob.net/wp-content/uploads/2020/09/Biaoqing-la-gi-Tong-hop-cac-hinh-anh-meme-gau.jpg"
              alt="Profile"
              srcset=""
            />
          </div>

          <div className={infoContent}>
            <Typography color="primary" align="center" spacing={3} variant="h6">
              Tài Khoản:<span className={infoSpacing}>{me.taiKhoan}</span>
            </Typography>
          </div>
          <div className={infoContent}>
            <Typography color="primary" align="center" spacing={3} variant="h6">
              Họ Tên:<span className={infoSpacing}>{me.hoTen}</span>
            </Typography>
          </div>
          <div className={infoContent}>
            <Typography color="primary" align="center" spacing={3} variant="h6">
              Email:<span className={infoSpacing}>{me.email}</span>
            </Typography>
          </div>
        </div>
      </Container>
    </Layout>
  );
};

export default Profile;
