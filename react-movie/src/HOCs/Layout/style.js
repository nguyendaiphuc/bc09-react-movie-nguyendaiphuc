import { makeStyles } from "@material-ui/styles";
const styles = makeStyles((theme) => {
  return {
    "@global": {
      a: {
        textDecoration: " none",
        color: "#FFF",
      },
    },
    footerList: {
      listStyle: "none",
      padding: "20px 0",
    },
    dFlex: {
      display: "flex",
      margin: "20px 0",
    },
    iconTheme: {
      marginRight: "10px",
    },
  };
});
export default styles;
