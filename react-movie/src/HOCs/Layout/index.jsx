import { Box, Container, Grid, Paper, Typography } from "@material-ui/core";
import React from "react";
import Header from "../../components/Header";
import FacebookIcon from "@material-ui/icons/Facebook";
import TwitterIcon from "@material-ui/icons/Twitter";
import InstagramIcon from "@material-ui/icons/Instagram";
import styles from "./style";
const Layout = (props) => {
  const classes = styles();
  const { footerList, iconTheme, dFlex } = classes;
  return (
    <Box bgcolor="#F3F0D7">
      <Header />
      {props.children}
      <Box bgcolor="#9D9D9D" color="#ffffff" paddingY="20px" marginTop="15px">
        <Container>
          <Grid container spacing={3}>
            <Grid item xs={3}>
              <ul className={footerList}>
                <li>
                  <Typography paddingY={2}>Contact</Typography>
                </li>
                <li>
                  <a className={dFlex}>
                    <FacebookIcon
                      className={iconTheme}
                      titleAccess="Facebook"
                    />
                    <span>Facebook</span>
                  </a>
                </li>

                <li>
                  <a className={dFlex}>
                    <TwitterIcon className={iconTheme} titleAccess="Twitter" />
                    <span>Twitter</span>
                  </a>
                </li>

                <li>
                  <a className={dFlex}>
                    <InstagramIcon
                      className={iconTheme}
                      titleAccess="Instagram"
                    />
                    <span>Instagram</span>
                  </a>
                </li>
              </ul>
            </Grid>
            <Grid item xs={3}>
              HI DEmo
            </Grid>
            <Grid item xs={3}>
              Hello Demo
            </Grid>

            <Grid item xs={3}>
              <Typography component="h3" variant="h4" align="center">
                My Movie
              </Typography>
            </Grid>
          </Grid>
        </Container>
      </Box>
    </Box>
  );
};

export default Layout;
