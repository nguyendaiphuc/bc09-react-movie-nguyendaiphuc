// import { withStyles } from "@material-ui/styles";
// import React, { Component } from "react";
// import style from "./style";
// import { NavLink } from "react-router-dom";
import React, { Fragment } from "react";

import {
  Card,
  CardActionArea,
  CardMedia,
  CardContent,
  Typography,
  CardActions,
  Button,
  makeStyles,
} from "@material-ui/core";
import { useHistory } from "react-router-dom";

const Course = ({ item }) => {
  const history = useHistory();
  const { maPhim, tenPhim, hinhAnh } = item;
  const useStyles = makeStyles(() => {
    return {
      imgDefault: {
        backgroundImage: `url(${hinhAnh}), url("http://movieapi.cyberlearn.vn/hinhanh/diep-van_gp01.jpg")`,
        color: "red",
        height: 400,
        borderBottom: "1px solid #231f20",
      },
      textColor: {
        height: 30,
      },
      buttonDetail: {
        color: "#cdc197",
        backgroundColor: "#231f20",
        fontWeight: "700",
        width: "100%",
        margin: "0 auto",
        transition: "all .5s ease-in-out",
        "&:hover": {
          color: "#231f20",
        },
      },
    };
  });
  const classes = useStyles();
  return (
    <Fragment>
      <Card>
        <CardActionArea>
          <CardMedia className={classes.imgDefault} title={tenPhim} />

          <CardContent>
            <Typography
              className={classes.textColor}
              gutterBottom
              variant="h6"
              component="h3"
            >
              {tenPhim}
            </Typography>
          </CardContent>
        </CardActionArea>
        <CardActions>
          <Button
            onClick={() => history.push(`/detail/${maPhim}`)}
            size="small"
            className={classes.buttonDetail}
          >
            Chi tiết
          </Button>
        </CardActions>
      </Card>
    </Fragment>
  );
};

export default Course;
