import React from "react";
import { NavLink, useHistory } from "react-router-dom";
import {
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  Button,
} from "@material-ui/core";
import MovieOutlinedIcon from "@material-ui/icons/MovieOutlined";
import useStyle from "./style";
import { useSelector } from "react-redux";
import PermIdentityIcon from "@material-ui/icons/PermIdentity";
const Header = () => {
  const history = useHistory();
  const classes = useStyle();
  const {
    title,
    navLink,
    active,
    appBar,
    appBarWrapper,
    colorTitle,

    appBarIcon,
  } = classes;
  const me = useSelector((state) => state.me);
  return (
    <AppBar className={appBar} elevation={0} position="sticky">
      <Toolbar className={appBarWrapper}>
        <IconButton
          onClick={() => {
            history.push("/");
          }}
          edge="start"
          aria-label="menu"
        >
          <MovieOutlinedIcon className={appBarIcon} />
        </IconButton>
        <Typography className={title} variant="h5">
          My <span className={colorTitle}> Movies Admin</span>
        </Typography>

        <NavLink
          className={navLink}
          exact
          activeClassName={active}
          to="/profile"
        >
          Profile
        </NavLink>
        <NavLink
          className={navLink}
          exact
          activeClassName={active}
          to="/userList"
        >
          Users
        </NavLink>

        <>
          <Button
            onClick={() => history.push("/profile")}
            style={{ marginLeft: 20 }}
          >
            {me?.hoTen}
            <PermIdentityIcon />
          </Button>
        </>
      </Toolbar>
    </AppBar>
  );
};

export default Header;
