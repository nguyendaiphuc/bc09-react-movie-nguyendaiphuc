import { makeStyles } from "@material-ui/core";

const styles = makeStyles((theme) => {
  return {
    "@global": {
      // Chỉnh cho tất cả các thẻ ở các component
    },
    appBarWrapper: {
      width: "85%",
      margin: "0 auto",
    },
    appBar: {
      background: "#f7dcdc",
      padding: "10px 0",
    },
    appBarVideo: {
      position: "absolute",
      top: "0",
      left: "0",
      zIndex: "2000",
    },
    title: {
      flexGrow: 1,
      fontWeight: "700",
    },
    appBarIcon: {
      fontSize: " 2rem",
      color: "#FF6F3D",
    },
    colorTitle: {
      color: "#FF6F3D",
    },
    navLink: {
      display: "flex",
      marginLeft: 50,
      color: "#000000",

      opacity: 0.5,
      fontWeight: "700",
      textDecoration: "none",
      "&:hover": {
        opacity: 1,
      },
      //max-width: 1280px
      [theme.breakpoints.down("md")]: {
        fontSize: 70,
      },
      //max-width: 960px
      [theme.breakpoints.down("sm")]: {
        fontSize: 50,
      },
      //max-width: 600px
      [theme.breakpoints.down("xs")]: {
        fontSize: 20,
      },
    },
    active: {
      opacity: 1,
      fontWeight: 700,
    },
  };
});
export default styles;
