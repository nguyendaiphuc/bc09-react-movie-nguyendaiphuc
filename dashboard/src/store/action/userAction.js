// tạo ra 1 async action để fetch ds khóa học
import axios from "axios";
import { createAction } from ".";
import { actionType } from "./type";
export const GetUser = async (dispatch) => {
  try {
    const res = await axios({
      url: `http://movieapi.cyberlearn.vn/api/QuanLyNguoiDung/LayDanhSachNguoiDung?MaNhom=GP01`,
      method: "GET",
    });
    dispatch(createAction(actionType.GET_USER, res.data.content));
  } catch (err) {
    console.log(err);
  }
};
