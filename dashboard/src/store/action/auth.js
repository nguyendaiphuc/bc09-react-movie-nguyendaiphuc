import axios from "axios";
import { createAction } from ".";
import { actionType } from "./type";

export const signin = (userLogin, callback) => {
  return async (dispatch) => {
    try {
      const res = await axios({
        method: "POST",
        url: "http://movieapi.cyberlearn.vn/api/QuanLyNguoiDung/DangNhap",
        data: userLogin,
      });
      // console.log(res);

      if (res.data.content.maLoaiNguoiDung === "QuanTri") {
        dispatch(createAction(actionType.SET_ME, res.data.content));
        localStorage.setItem("t", res.data.content.accessToken);
        alert("Đăng nhập Thành Công");
      } else {
        alert("Bạn Không Có quyền đăng nhập vào trang Quản trị");
      }

      callback();
      console.log(res);
    } catch (err) {
      alert(err.response.data.content);
    }
  };
};

export const fetchMe = async (dispatch) => {
  try {
    const res = await axios({
      url: "http://movieapi.cyberlearn.vn/api/QuanLyNguoiDung/ThongTinTaiKhoan",
      method: "POST",
      headers: {
        Authorization: "Bearer " + localStorage.getItem("t"),
      },
    });
    // console.log(res);
    dispatch(createAction(actionType.SET_ME, res.data.content));
  } catch (err) {
    console.log(err);
  }
};
