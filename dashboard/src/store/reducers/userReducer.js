import { actionType } from "../action/type";

const initialState = {
  userList: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionType.GET_USER:
      state.userList = action.payload;
      return { ...state };

    default:
      return state;
  }
};
export default reducer;
