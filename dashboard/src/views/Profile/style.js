import { makeStyles } from "@material-ui/core";
const styles = makeStyles(() => {
  return {
    "@global": {
      boxSizing: "border-box",
    },
    wrapper: {
      height: "80vh",
      alignItems: "center",
      textAlign: "center",
      margin: "0 auto",
    },
    img: {
      width: "200px",
      height: "200px",
      borderRadius: "50%",
    },
    userImg: {},
    infoContent: {
      paddingTop: "20px",
      width: "50%",
      margin: "0 auto",
      marginTop: "30px",
    },

    infoSpacing: {
      margin: "0 30px",
      color: "#345B63",
    },
  };
});
export default styles;
