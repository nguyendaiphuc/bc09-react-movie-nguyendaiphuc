import React, { useCallback, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { GetUser } from "../../store/action/userAction";
import Layout from "../../HOCs/Layout/index";
// import { DataGrid } from "@material-ui/data-grid";
import { makeStyles } from "@material-ui/core/styles";

import {
  Container,
  Paper,
  TableRow,
  TableHead,
  TableContainer,
  TableCell,
  TableBody,
  Table,
  TablePagination,
  Button,
} from "@material-ui/core";

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
  tableContent: {
    borderRadius: "20px",
    padding: "0 20px",
  },
  tableHeader: {
    fontWeight: "bold",
    fontSize: "1.1rem",
  },
  tablePage: {
    background: "#fff",
    width: "100%",
    borderRadius: "20px",
    textAlign: "right",
    margin: "0 auto",
  },
});

const UserList = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { userList } = useSelector((state) => state.userReducer);
  const getUserList = useCallback(() => {
    dispatch(GetUser);
  }, [dispatch]);

  useEffect(() => {
    getUserList();
  }, [getUserList]);

  // const columns = [
  //   { field: "id", headerName: "ID" },
  //   { field: "hoTen", headerName: "Họ Tên" },
  //   { field: "soDt", headerName: "Số Điện Thoại", width: 300 },
  //   { field: "email", headerName: "Email", with: 300 },
  //   { field: "maLoaiNguoiDung", headerName: "Loại Người Dùng" },
  // ];
  // const rows = [
  //   { id: 123 },
  //   { email: "stringg2709@gmail.com" },
  //   { hoTen: "pham minh" },
  //   { maLoaiNguoiDung: "KhachHang" },
  //   { matKhau: "1234567324" },
  //   { soDt: "0900000002" },
  // ];

  // useEffect(() => {
  //   setTableData(userList);
  // }, [userList]);
  // console.log(tableData);

  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [page, setPage] = useState(0);
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const detUser = () => {
    alert("Hiện Tại chưa thể Xóa User");
  };
  return (
    <Layout>
      <Container>
        <div style={{ margin: "20px 0" }}>
          <TableContainer component={Paper} className={classes.tableContent}>
            <Table className={classes.table} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell className={classes.tableHeader}>Họ Tên</TableCell>
                  <TableCell className={classes.tableHeader} align="left">
                    Số điện Thoại
                  </TableCell>
                  <TableCell className={classes.tableHeader} align="left">
                    Email
                  </TableCell>
                  <TableCell className={classes.tableHeader} align="left">
                    Loại Người dùng
                  </TableCell>
                  <TableCell className={classes.tableHeader} align="left">
                    Hành Động
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {userList
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row) => (
                    <TableRow key={row.hoTen}>
                      <TableCell>{row.hoTen}</TableCell>
                      <TableCell align="left">{row.soDt}</TableCell>
                      <TableCell align="left">{row.email}</TableCell>
                      <TableCell align="left">{row.maLoaiNguoiDung}</TableCell>
                      <TableCell align="left">
                        <Button variant="contained" color="primary">
                          Sửa
                        </Button>
                        <Button
                          onClick={detUser}
                          variant="contained"
                          color="secondary"
                        >
                          Xóa
                        </Button>
                      </TableCell>
                    </TableRow>
                  ))}
              </TableBody>
            </Table>
          </TableContainer>
          <div className={classes.tablePage}>
            <TablePagination
              rowsPerPageOptions={[5, 10, 25]}
              component="div"
              count={userList.length}
              rowsPerPage={rowsPerPage}
              page={page}
              onPageChange={handleChangePage}
              onRowsPerPageChange={handleChangeRowsPerPage}
            />
          </div>

          {/* <DataGrid rows={rows} columns={columns} pageSize={10} checkboxSelection /> */}
        </div>
      </Container>
    </Layout>
  );
};

export default UserList;
