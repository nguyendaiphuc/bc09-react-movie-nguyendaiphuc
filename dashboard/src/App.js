import React, { Component } from "react";

import { Signin } from "./views/Signin";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import { connect } from "react-redux";
import { fetchMe } from "./store/action/auth";
import { AuthRoute, PrivateRoute } from "./HOCs/Route";
import Profile from "./views/Profile";
import UserList from "./views/UserList/index.jsx";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={Signin} />

          {/* <AuthRoute path="/signup" component={Signup} redirectPath="/" /> */}
          <PrivateRoute path="/profile" component={Profile} />
          <PrivateRoute path="/userList" component={UserList} />
        </Switch>
      </BrowserRouter>
    );
  }
  componentDidMount() {
    const token = localStorage.getItem("t");
    if (token) this.props.dispatch(fetchMe);
  }
}

export default connect()(App);
